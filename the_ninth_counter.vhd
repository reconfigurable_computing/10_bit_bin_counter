library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity the_ninth_counter is
port (
	clk   : in std_logic;
	BTN	: in std_logic;
	SW		: in std_logic_vector (1 downto 0);
	LED   : out std_logic_vector (9 downto 0)
);
end the_ninth_counter;

architecture Behavioral of the_ninth_counter is
begin
	process (clk, BTN, SW)
		variable counter 	: integer := 0;
		variable binary 	: unsigned (9 downto 0) := "0000000000";
		variable time_val	: integer := 50000000;
	begin
		if rising_edge(clk) then
			counter := counter + 1;
			if (counter = time_val) then
				counter := 0;
				binary := binary + "1";
				LED <= std_logic_vector(binary);
			end if;
		end if;
		if (BTN = '0') then
			binary := "0000000000";
			LED <= std_logic_vector(binary);
		end if;
	end process;
end Behavioral;